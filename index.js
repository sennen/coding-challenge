const axios = require('axios');
const bluebird = require('bluebird');
const moment = require('moment');

const R = require('ramda');

const {
    SUNRISE_SUNSET_API_ENDPOINT,
    API_CONCURRENCY,
    DEFAULT_COORD_CNT,
    FETCH_DATA_CNT,
    IS_RETURN_FORMATTED,
} = require('./constant');
const { getRandomNumByRange } = require('./utils/math');

const getLatitude = (rangeFrom = -90, rangeTo = 90, fixedPoint = 7) => {
    return getRandomNumByRange(rangeFrom, rangeTo, fixedPoint);
};

const getLongitude = (rangeFrom = -180, rangeTo = 180, fixedPoint = 7) => {
    return getRandomNumByRange(rangeFrom, rangeTo, fixedPoint);
};

const getCoordinatesArr = (num = DEFAULT_COORD_CNT) => {
    const coordinatesArr = [];
    while (coordinatesArr.length < num) {
        const newLat = getLatitude(0, 45);
        const newLng = getLongitude(0, 90);

        // make sure no duplicated coordinate pair
        const coordinateExist = coordinatesArr.some(
            (co) => co.latitude === newLat && co.longitude === newLng
        );
        if (!coordinateExist) {
            coordinatesArr.push({
                latitude: newLat,
                longitude: newLng,
            });
        }
    }

    return coordinatesArr;
};

// API Documentation - https://sunrise-sunset.org/api
const fetchSunriseSunsetData = async (requestObj) => {
    const queryParams = new URLSearchParams(requestObj);
    const reqUri = `${SUNRISE_SUNSET_API_ENDPOINT}?${queryParams.toString()}`;
    try {
        const response = await axios.get(reqUri);
        return {
            fetchSuccess: true,
            payload: response.data,
        };
    } catch (err) {
        // console.error(err);
        return {
            fetchSuccess: false,
            message: err.message,
            payload: err.response.data,
        };
    }
};

const processSunriseSunsetData = async (reqPayload) => {
    const response = await fetchSunriseSunsetData(reqPayload);

    if (response.fetchSuccess && response.payload.status === 'OK') {
        return {
            isErr: false,
            result: {
                latitude: reqPayload.lat,
                longitude: reqPayload.lng,
                ...response.payload.results,
            },
        };
    } else {
        return {
            isErr: true,
            result: {
                latitude: reqPayload.lat,
                longitude: reqPayload.lng,
                errMsg: response.payload.status,
            },
        };
    }
};

const _sortDiffFunc = (isFormattedData, a, b) => {
    return isFormattedData
        ? new Date(moment(a.sunrise, 'hh:mm:ss a')) - new Date(moment(b.sunrise, 'hh:mm:ss a'))
        : new Date(a.sunrise) - new Date(b.sunrise);
};

const sortSunriseData = (inputArr, isFormattedData, order = 1) => {
    switch (order) {
        // ascending
        case 1:
            return [...inputArr].sort((a, b) => {
                return _sortDiffFunc(isFormattedData, a, b);
            });
        case -1:
            return [...inputArr].sort((a, b) => {
                return _sortDiffFunc(isFormattedData, b, a);
            });
    }
};

const resultMsg = ({ latitude, longitude, sunrise, day_length }, descStr, isFormatted) => {
    console.log(
        `(latitude, longitude) = (${latitude}, ${longitude}) has the ${descStr} sunrise among the data 
which is at ${sunrise} 
with its corresponding day length = ${day_length} ${isFormatted ? '' : 'seconds'}
`
    );
};

// main
(async () => {
    const sunriseSunsetDataArr = [];
    const errDataArr = [];

    // to return formatted data, set to 1, else 0
    const isReturnFormattedData = IS_RETURN_FORMATTED;
    const numOfData = FETCH_DATA_CNT;

    try {
        // const coordinatesArr = getCoordinatesArr(8);
        const coordinatesArr = getCoordinatesArr(numOfData);

        await bluebird.map(
            coordinatesArr,
            async (coordinates) => {
                const { isErr, result } = await processSunriseSunsetData({
                    lat: coordinates.latitude,
                    lng: coordinates.longitude,
                    formatted: isReturnFormattedData,
                });
                if (isErr) {
                    errDataArr.push(result);
                } else {
                    sunriseSunsetDataArr.push(result);
                }
            },
            { concurrency: API_CONCURRENCY }
        );

        console.log(
            `${numOfData} data fetched; ${sunriseSunsetDataArr.length} successful; ${errDataArr.length} failed`
        );

        const sortedSunriseDataAsc = sortSunriseData(sunriseSunsetDataArr, isReturnFormattedData);
        const earliestSunriseEnt = sortedSunriseDataAsc[0];
        resultMsg(earliestSunriseEnt, 'earliest', isReturnFormattedData);

        // const sortetSunriseDataDesc = sortSunriseData(sunriseSunsetDataArr, isReturnFormattedData, -1);
        // const latestSunriseEnt = sortetSunriseDataDesc[0];
        // resultMsg(latestSunriseEnt, 'latest', isReturnFormattedData);
        process.exit(0);
    } catch (err) {
        console.error(err);
        process.exit(1);
    }
})();
