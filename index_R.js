const axios = require('axios');
const pLimit = require('p-limit');
const moment = require('moment');

const R = require('ramda');

const {
    SUNRISE_SUNSET_API_ENDPOINT,
    API_CONCURRENCY,
    DEFAULT_COORD_CNT,
    FETCH_DATA_CNT,
    IS_RETURN_FORMATTED,
} = require('./constant');
const { getRandomNumByRange } = require('./utils/math');

const getLatitude = (rangeFrom = -90, rangeTo = 90, fixedPoint = 7) => {
    return getRandomNumByRange(rangeFrom, rangeTo, fixedPoint);
};

const getLongitude = (rangeFrom = -180, rangeTo = 180, fixedPoint = 7) => {
    return getRandomNumByRange(rangeFrom, rangeTo, fixedPoint);
};

const getCoordinatesUriArr = (num = DEFAULT_COORD_CNT, requestObj = {}) => {
    const coordinatesArr = [];
    while (coordinatesArr.length < num) {
        const newLat = getLatitude(0, 45);
        const newLng = getLongitude(0, 90);

        // make sure no duplicated coordinate pair
        const coordinateExist = coordinatesArr.some(
            (co) => co.latitude === newLat && co.longitude === newLng
        );
        if (!coordinateExist) {
            const newRequestObj = {
                ...requestObj,
                lat: newLat,
                lng: newLng,
            };
            const queryParams = new URLSearchParams(newRequestObj);
            const reqUri = `${SUNRISE_SUNSET_API_ENDPOINT}?${queryParams.toString()}`;
            coordinatesArr.push({
                latitude: newLat,
                longitude: newLng,
                reqUri,
            });
        }
    }

    return coordinatesArr;
};

// API Documentation - https://sunrise-sunset.org/api
const fetchSunriseSunsetData = async (reqUri) => {
    try {
        const response = await axios.get(reqUri);
        return {
            fetchSuccess: true,
            payload: response.data,
        };
    } catch (err) {
        // console.error(err);
        return {
            fetchSuccess: false,
            message: err.message,
            payload: err.response.data,
        };
    }
};

const processSunriseSunsetData = async (reqPayload) => {
    const response = await fetchSunriseSunsetData(reqPayload.reqUri);

    if (response.fetchSuccess && response.payload.status === 'OK') {
        return {
            isErr: false,
            result: {
                latitude: reqPayload.latitude,
                longitude: reqPayload.longitude,
                ...response.payload.results,
            },
        };
    } else {
        return {
            isErr: true,
            result: {
                latitude: reqPayload.latitude,
                longitude: reqPayload.longitude,
                errMsg: response.payload.status,
            },
        };
    }
};

const promisesWithConcurrencyLimit = (inputArr, limit) => {
    return inputArr.map((ent) => {
        return limit(() => processSunriseSunsetData(ent));
    });
};

// const _sortDiffFunc = (isFormattedData, a, b) => {
//     return isFormattedData
//         ? new Date(moment(a.sunrise, 'hh:mm:ss a')) - new Date(moment(b.sunrise, 'hh:mm:ss a'))
//         : new Date(a.sunrise) - new Date(b.sunrise);
// };

// const sortSunriseData = (inputArr, isFormattedData, order = 1) => {
//     switch (order) {
//         // ascending
//         case 1:
//             return [...inputArr].sort((a, b) => {
//                 return _sortDiffFunc(isFormattedData, a, b);
//             });
//         case -1:
//             return [...inputArr].sort((a, b) => {
//                 return _sortDiffFunc(isFormattedData, b, a);
//             });
//     }
// };

const sortByPropFuncByR = (propToBeSorted, order = 1) => {
    switch (order) {
        // ascending
        case 1:
            return R.sortWith([R.ascend(R.prop(propToBeSorted))]);
        case -1:
            return R.sortWith([R.descend(R.prop(propToBeSorted))]);
    }
};

const resultMsg = ({ latitude, longitude, sunrise, day_length }, descStr, isFormatted) => {
    console.log(
        `(latitude, longitude) = (${latitude}, ${longitude}) has the ${descStr} sunrise among the data 
which is at ${sunrise} 
with its corresponding day length = ${day_length} ${isFormatted ? '' : 'seconds'}
`
    );
};

// main
(async () => {
    // to return formatted data, set to 1, else 0
    const isReturnFormattedData = IS_RETURN_FORMATTED;
    const limit = pLimit(API_CONCURRENCY);
    const numOfData = FETCH_DATA_CNT;

    try {
        const coordinatesUriArr = getCoordinatesUriArr(numOfData, {
            formatted: isReturnFormattedData,
        });

        const resultArr = await Promise.all(promisesWithConcurrencyLimit(coordinatesUriArr, limit));

        // get data array
        const sunriseSunsetDataArr = R.pluck(
            'result',
            R.filter((ent) => !ent.isErr, resultArr)
        );

        // get error array (if any)
        const errDataArr = R.pluck(
            'result',
            R.filter((ent) => ent.isErr, resultArr)
        );

        console.log(
            `${numOfData} data fetched; ${sunriseSunsetDataArr.length} successful; ${errDataArr.length} failed`
        );

        // const sortedSunriseDataAsc = sortSunriseData(sunriseSunsetDataArr, isReturnFormattedData);
        // const earliestSunriseEnt = sortedSunriseDataAsc[0];
        // resultMsg(earliestSunriseEnt, 'earliest', isReturnFormattedData);

        const sortFuncAscByR = sortByPropFuncByR('sunrise', 1);
        const sortedSunriseDataAscByR = sortFuncAscByR(
            sunriseSunsetDataArr.map((ent) => {
                return {
                    ...ent,
                    sunrise: isReturnFormattedData
                        ? moment(ent.sunrise, 'hh:mm:ss A')
                        : ent.sunrise,
                };
            })
        ).map((res) => {
            return {
                ...res,
                sunrise: isReturnFormattedData ? res.sunrise.format('hh:mm:ss A') : res.sunrise,
            };
        });

        const earliestSunriseEntByR = sortedSunriseDataAscByR[0];

        resultMsg(earliestSunriseEntByR, 'earliest (by R)', isReturnFormattedData);

        process.exit(0);
    } catch (err) {
        console.error(err);
        process.exit(1);
    }
})();
