// Ref: https://stackoverflow.com/questions/6878761/javascript-how-to-create-random-longitude-and-latitudes
const getRandomNumByRange = (rangeFrom, rangeTo, fixedPoint) => {
    return (Math.random() * (rangeTo - rangeFrom) + rangeFrom).toFixed(fixedPoint) * 1;
};

module.exports = {
    getRandomNumByRange,
};
