# coding-challenge

> Fetch sunrise & sunset times of 100 locations and get earliest sunrise time and its corresponding day length

## index.js

> Version 1 - Not using Ramda
> To run this version, type `npm start`

## index_R.js

> Version 2 - Try to implement some Ramda functions
> To run this version, type `node index_R.js`
